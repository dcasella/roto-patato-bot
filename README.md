# Roto Patato Bot

Telegram Bot to retrieve movie ratings from IMDB, Metacritic and Rotten Tomatoes (\*).  
Built on the [Python Telegram Bot](https://github.com/python-telegram-bot/python-telegram-bot) wrapper.  
Movie search is based on the [TMDB API](https://github.com/celiao/tmdbsimple/) wrapper.  
Movie ratings are retrieved from the [OMDB API](http://omdbapi.com).  

(\*) Rotten Tomatoes API Keys are somewhat impossible to obtain for personal projects; the Bot only delivers links to each movie's page.  

## Commands

Search for a movie with title \<movie-title>.  
Optional: specify the \<release-year> of the movie.  

```
/search <movie-title> [<release-year>]
```

or

```
/s <movie-title> [<release-year>]
```
&nbsp;

Get the list of movies released in the last 2 months.  

```
/current
```

or

```
/c
```
&nbsp;

## Examples

```
/s x-men

X-Men: Apocalypse (2016)
Rotten Tomatoes (http://www.rottentomatoes.com/m/x_men_apocalypse/)
Metacritic: 5.2
IMDB: 7.1
```

```
/s x-men 2000

X-Men (2000)
Rotten Tomatoes (http://www.rottentomatoes.com/m/xmen/)
Metacritic: 6.4
IMDB: 7.4
```

