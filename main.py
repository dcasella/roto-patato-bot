#!/usr/bin/env python
# pylint: disable=I0011, C0411, E0401, C0111, W0613, C0103
# -*- coding: utf-8 -*-

"""Roto Patato Bot

Description:
Search for a movie with /search <title> (any language).
Get the lastest movies with /current.

About:
Search for movie scores or get the lastest movies list.

Commands:
start - Start conversation with Roto Patato
search - Search movie title (or year too)
current - Get the lastest movies
"""


import json
import re
import logging
import requests
from datetime import datetime, date

import tmdbsimple as tmdb
from telegram import ChatAction
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from dateutil.relativedelta import relativedelta


BOT_NAME = 'Roto Patato'


# Load API Keys file

with open('api_keys.json') as api_keys:
    KEYS = json.load(api_keys)


# Config

updater = Updater(token=KEYS['telegram'])
dispatcher = updater.dispatcher
tmdb.API_KEY = KEYS['tmdb']
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


# Debug

def log(update):
    logging.info('[' + str(update.message.chat_id) + '] '
                 + update.message.chat.username + ': '
                 + update.message.text)


# Helpers

def message(bot, update, string):
    bot.sendMessage(chat_id=update.message.chat_id, text=string,
                    parse_mode='MARKDOWN', disable_web_page_preview=True)


def typing(bot, update):
    bot.sendChatAction(chat_id=update.message.chat_id,
                       action=ChatAction.TYPING)


def sort_key(param):
    return (param['popularity'], param['release_date'], param['release_date'] == '')


def round_str(param):
    return str(round(param, 1))


def div10_str(param):
    return round_str(float(param) / 10)


def parse_scores(mc_rating, rt_rating, rt_url, imdb_rating):
    empty_value = ('N/A', '', 0)

    if mc_rating in empty_value:
        mc_score = 'Metacritic: *N/A*\n'
    else:
        mc_score = 'Metacritic: *' + div10_str(mc_rating) + '*\n'

    if rt_rating in empty_value and rt_url not in empty_value:
        rt_score = '[Rotten Tomatoes](' + rt_url + ')\n'
    elif rt_url in empty_value:
        rt_score = 'Tomatometer: *N/A*\n'
    else:
        rt_score = 'Tomatometer: *' + div10_str(rt_rating) + '*\n'

    if imdb_rating in empty_value:
        imdb_score = 'IMDB: *N/A*'
    else:
        imdb_score = 'IMDB: *' + imdb_rating + '*'

    return {
        'mc':   mc_score,
        'rt':   rt_score,
        'imdb': imdb_score
    }


def parse_movie(movie):
    omdb_url = 'http://www.omdbapi.com/'

    title = movie['title']
    year = movie['release_date'][0:4]
    omdb_movie = requests.get(omdb_url
                              + '?t=' + title
                              + '&y=' + year
                              + '&type=movie'
                              + '&tomatoes=true').json()

    print(' ' * 26
          + 'OMDB - '
          + omdb_url
          + '?t=' + title
          + '&y=' + year
          + '&type=movie'
          + '&tomatoes=true')

    header = '*' + title + '* (' + year + ')'

    if omdb_movie['Response'] != 'False':
        mc_rating = omdb_movie['Metascore']
        rt_rating = omdb_movie['tomatoMeter']
        rt_url = omdb_movie['tomatoURL']
        imdb_rating = omdb_movie['imdbRating']
        scores = parse_scores(mc_rating, rt_rating, rt_url, imdb_rating)

        return header + '\n' + scores['rt'] + scores['mc'] + scores['imdb'] + '\n'

    return header + '\nNo data found.\n'


def release_date(release):
    return datetime.strptime(release, '%Y-%m-%d').date()


def remove_old(movies, gte):
    return [movie for movie in movies
            if release_date(movie['release_date']) >= gte]


def build_request(args):
    full = ' '.join(args)
    title = ''
    year = None

    for arg in args:
        if re.match(r'\d{4}', arg):
            year = int(arg)
        else:
            title += ' ' + arg

    return {
        "full": full,
        "title": title,
        "year": year
    }


# Commands

def echo(bot, update):
    log(update)


def start(bot, update):
    log(update)
    message(bot, update,
            'Welcome to *' + BOT_NAME + '*!\n'
            'Use `/search <title>` or `/s <title>` to search for a movie.\n'
            'Use `/current` or `/c` to receive the lastest movies.')


def search(bot, update, args):
    log(update)
    typing(bot, update)

    if not args:
        message(bot, update, "Enter a movie title")
        return

    request = build_request(args)

    tmdb_search = tmdb.Search().movie(query=request['full'])
    tmdb_movies = sorted(tmdb_search['results'], key=sort_key, reverse=True)

    if tmdb_movies:
        response = parse_movie(tmdb_movies[0])
        message(bot, update, response)
    elif request['year']:
        tmdb_search = tmdb.Search().movie(query=request['title'],
                                          year=request['year'])
        tmdb_movies = sorted(tmdb_search['results'], key=sort_key,
                             reverse=True)

        if tmdb_movies:
            response = parse_movie(tmdb_movies[0])
            message(bot, update, response)
        else:
            message(bot, update, "No movies found.")
    else:
        message(bot, update, "No movies found.")


def current(bot, update):
    log(update)
    typing(bot, update)

    lte = date.today()
    gte = lte + relativedelta(months=-2)

    tmdb_search = tmdb.Discover().movie(sort_by='popularity.desc',
                                        release_date_gte=gte,
                                        release_date_lte=str(lte))
    tmdb_movies = sorted(remove_old(tmdb_search['results'], gte), key=sort_key,
                         reverse=True)

    response = '\n'.join([parse_movie(movie) for movie in tmdb_movies])
    message(bot, update, response)


# Handlers

echo_handler = MessageHandler(Filters.text, echo)
dispatcher.add_handler(echo_handler)

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

search_handler = CommandHandler('search', search, pass_args=True)
s_handler = CommandHandler('s', search, pass_args=True)
dispatcher.add_handler(search_handler)
dispatcher.add_handler(s_handler)

current_handler = CommandHandler('current', current)
c_handler = CommandHandler('c', current)
dispatcher.add_handler(current_handler)
dispatcher.add_handler(c_handler)


# Polling

while True:
    try:
        updater.start_polling(poll_interval=1.0, timeout=60)
    except ReadTimeoutError:
        continue


# Stop

updater.idle()
